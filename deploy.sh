#!/bin/bash
# project variables
PNAME=""
STAGINGURL=""
LIVEURL=""
USERNAME=$(whoami)
TYPE=$1
DEPLOYDB=$2

if [ "$TYPE" == "staging" ]; then
    echo -e "\npushing site to staging\n"
    dandelion --config=deploy/staging.yml deploy

    if [ "$DEPLOYDB" == "pushdb" ]; then
        echo -e "\npushing database to staging\n"
        echo -e "backing up exisitng database and importing new \n"
        curl -s --data key=longstring $STAGINGURL'/db.php'
        echo -e ""
    fi
fi

if [ "$TYPE" == "live" ]; then
    echo -e "\npushing site to live\n"
    dandelion --config=deploy/live.yml deploy --dry-run
fi

exit