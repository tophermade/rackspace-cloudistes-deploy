This being built for rackspace cloud sites, it's a bit complicated.Uses dandelion for deploying changed files.

1) open deploy.sh and update PNAME, STAGINGURL, and LIVEURL with the correct information. PNAME == project name

2) open db.php and change $username, $password, $hostname, and $dbname to reflect your site.

3) change $thekey in db.php and the key portion of key= on line 17 of deploy.sh to a long random string.

4) move db.php into the webroot of your project

5) configure staging.yml and live.yml according the the dandelion docs

6) If you have a database you need pushed, place it in the same directory as deploy.sh and name it db.sql (databases can only be deloyed to staging, to prevent accidents).

From terminal you may run:
./deploy.sh staging (deploys to staging
./deploy.sh live (deploys to live
./deploy.sh staging pushdb (deploys to staging,and imports .db.sql

What happens with a push is dandelion pushes the latest commit to the server. If you do "pushdb", the database on the server will be backedup, placed in ../dbbackup.php and the db.sql file will be imported, and then it will be deleted.